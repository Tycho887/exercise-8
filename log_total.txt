commit b5a5640bfdd24bff688c60a0aa0b9e3f5219a7ee
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Fri Nov 17 16:36:33 2023 +0100

    Fixed up the code somewhat

commit a3ab4a21b92636bc018bf33b87ff6f9e0efac27b
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Fri Nov 17 16:30:11 2023 +0100

    Fixed buf

commit 6e8a9c2789022d3c1f8ee4d561bd1da8ef814836
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Fri Nov 17 16:27:47 2023 +0100

    fixed search algorithm

commit faf1fc57409896f1d283a919373b5d4ee1a0e29b
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Fri Nov 17 15:25:59 2023 +0100

    Added support for mesh visualization

commit d0eeb81fb66a71e73acb37b778fe9d72ad9ce576
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Fri Nov 17 15:25:18 2023 +0100

    Added support for mesh visualization

commit e914909f6fad91e798a057b72a6bf9685ce13e79
Author: Fabian Heflo <fheflo@gmail.com>
Date:   Fri Nov 17 15:04:23 2023 +0100

    trying to visulize mesh

commit c14bb1e6d633ab10bad2f1c0ca954e2ebcd46bf3
Merge: b9ae314 a36a47b
Author: Michael Johansen <michael.ruben.johansen@nmbu.no>
Date:   Fri Nov 17 13:33:50 2023 +0000

    Merge branch 'Task_2' into 'main'
    
    Task 2
    
    See merge request Tycho887/exercise-8!1

commit a36a47bf8c41295c0477dfd741ad356b84aa7bae
Author: Fabian Heflo <fheflo@gmail.com>
Date:   Fri Nov 17 14:30:21 2023 +0100

    fixed time

commit 2a4c71bece82f41fb6ee80387210e14b4ed68a74
Author: Fabian Heflo <fheflo@gmail.com>
Date:   Thu Nov 16 22:36:45 2023 +0100

    Improved timing

commit 0c3b0b89f2164af3c6412f23ef1bbd329a91c60a
Author: Fabian Heflo <fheflo@gmail.com>
Date:   Thu Nov 16 22:29:28 2023 +0100

    timed function

commit 78777c82655cff2e5d35e8108d77431cb62dcce6
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Wed Nov 15 10:01:41 2023 +0100

    Update to reduce time taken

commit 90bfd9055f43a10d7fac53fd5a34aa60daf7ff5b
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Wed Nov 15 09:18:14 2023 +0100

    Added searchspace reduction optimazation

commit b65e8178eb5c938eb4a7c640fd86f01de861d9c9
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Wed Nov 15 09:12:11 2023 +0100

    Simplified the search algorithm

commit 15440a3d36c18a79b4c08110c77c2fd971cd3fc4
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Wed Nov 15 09:11:54 2023 +0100

    Simplified the search algorithm

commit dd4250b657f1052c42b1c15635265b53c30716aa
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Wed Nov 15 09:08:11 2023 +0100

    Added optimatization documentation

commit 2bca7324558378e7824b846d20e3d22330be27e0
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Wed Nov 15 09:03:37 2023 +0100

    Added support for compact queries

commit cfb1bc630f16bba6241829da6819e1894695ca08
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 19:02:58 2023 +0100

    final update to the program

commit 07304f5880a595a4f67e074363814bbb8ae93251
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 18:59:33 2023 +0100

    Added authors

commit ffc9397df842f40d9117f53383647b4baae0c385
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 18:54:14 2023 +0100

    update

commit 9e928af5a5527987e84794150c145f9e9f149f11
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 18:52:49 2023 +0100

    update

commit 0ce9f0abcac93d7fa94495b990a2be215ee418fc
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 18:51:54 2023 +0100

    Siste arbeid p├Ñ tirsdag

commit 4a23278ddd8b1ec0abe5eb64bd6b2429d4220601
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 18:45:06 2023 +0100

    Siste arbeid p├Ñ tirsdag

commit d164ffe1ac8fac13580519a7d403580ab1a8f353
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 18:36:10 2023 +0100

    Fixing bugs and making the program better

commit 15188d772b163d4ad923c9215ec5631e9b43f727
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 18:35:49 2023 +0100

    Fixing bugs in the program

commit ce2de23f4b3fa97702f721f1d2e8e8c940322440
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 18:27:16 2023 +0100

    Videre arbeid p├Ñ assignment 8

commit b9ae314f481e751db19ab3b87ad34775b8947274
Author: Tycho887 <michaelrj03@gmail.com>
Date:   Tue Nov 14 17:38:14 2023 +0100

    First work

commit 9a50bfedb713814fa8344ac40d617e32e085718b
Author: Michael Johansen <michael.ruben.johansen@nmbu.no>
Date:   Tue Nov 14 13:55:32 2023 +0000

    Initial commit
